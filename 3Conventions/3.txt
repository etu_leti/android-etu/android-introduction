class DateRange(val start: MyDate, val end: MyDate) : Iterable<MyDate> {
    override fun iterator(): Iterator<MyDate> {
        return DateRangeIterator(start, end)
    }

    private class DateRangeIterator(private val start: MyDate, private val end: MyDate) : Iterator<MyDate> {
        private var current: MyDate = start

        override fun hasNext(): Boolean = current <= end

        override fun next(): MyDate {
            if (!hasNext()) throw NoSuchElementException()
            val result = current
            current = current.followingDate()
            return result
        }
    }
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in firstDate..secondDate) {
        handler(date)
    }
}